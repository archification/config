## source this with . setup.sh

souce bashrc

echo -n $clear
echo -n $'\033[31m'

cat <<'EOM'
               ___
              | __|__ _ ____  _
              | _|/ _` (_-< || |
            __|___\__,_/__/\_, |_____              _           _
           | _ ) __ _ __| ||__/|_   _|__ _ _ _ __ (_)_ _  __ _| |
           | _ \/ _` (_-< ' \    | |/ -_) '_| '  \| | ' \/ _` | |
           |___/\__,_/__/_||_| _ |_|\___|_| |_|_|_|_|_||_\__,_|_|
               |  \/  |__ _ __| |_ ___ _ _
               | |\/| / _` (_-<  _/ -_) '_|
               |_|  |_\__,_/__/\__\___|_|      [95mv1.0.0  [34m@rwxrob

EOM

declare -i major=${BASH_VERSION:0:1}
if [[ ! ( $major == "4" || $major == "5" ) ]]; then
  echo Sorry, requires Bash 4+. Install and try again.
  return
fi

# TODO clean this up once we have all the components ported to bash
# modular scripts

source bashrc.d/100-platform-detection.bash
source bashrc.d/102-path.bash
source bashrc.d/104-shopt.bash
source bashrc.d/106-temp.bash
source bashrc.d/108-shortcuts-dir.bash


telln <<'EOM'
  **Welcome!** This `setup` script will walk you through the steps to
  setup the terminal master suite, Bash, Vim, TMUX, and Lynx. Each step
  will include a "confirmation" prompt. Even though existing files and
  links are moved and renamed rather than deleted when you agree that
  action will happen and *cannot* be undone by this script, so ***please
  read each prompt***.  Most are harmless enough and usually the defaults
  are ok.  You can cancel setup anytime with Ctrl-C.

  *Note that nothing will be installed*, only symbolic links will be created.
  To install everything check the `install` script.
EOM

echo

################################### Mac ##################################

if [[ $PLATFORM = mac ]]; then
  if ! havecmd gls; then
    warnln 'Need to `brew install coreutils` for Mac to work.'
  fi
fi

################################## Bash ##################################

declare -A links=(
  [inputrc]=inputrc
  [bashrc]=bashrc
  [fonts]=fonts
  [bashrc]=bashrc
  [bash_profile]=profile
  [profile]=profile
  [dircolors]=dircolors
)

tell '**Setup Bash?**'

if confirm; then

for from in "${!links[@]}"; do
  symlink "$HOME/.$from" "$PWD/${links[$from]}"
done

echo

remindln <<'EOM'
Don't forget to create a `.bash_personal` and/or a `.bash_private`,
which should usually be from their own repos. See the following repos
for examples of how to make your own:

 <https://gitlab.com/rwxrob/config-sample-personal>
 <https://gitlab.com/rwxrob/config-sample-private>

You don't need these right away, but eventually you will.

You might also want to create symlinks from ~/.config subdirectories
to your own private repo directories in order to preserve your
application settings (ex: ~/.config/inkscape -> $PRIVATE/inkscape).
EOM

echo

fi

########################### Screen-centric TMUX ##########################

tell '**Setup TMUX?**'

if confirm; then
  type tmux
  if [[ $? == 0 ]]; then
    symlink "$HOME/.tmux.conf" "$PWD/tmux.conf"
  else
    warnln "TMUX isn't installed. Skipping."
  fi
    echo
fi

################################## More ##################################

# TODO add an interactive prompt for this stuff

mkdir -p "$HOME/.config/alacritty"
symlink "$HOME/.config/alacritty" "$PWD/alacritty"

mkdir -p "$HOME/.config/lynx"
symlink "$HOME/.config/lynx" "$PWD/lynx"


echo

telln <<'EOM'
  **All done.** Type the following command to reset your currently running
  bash terminal session to a new one using all your new setting. In fact,
  remember this command. It will save lots of time resetting every time
  you make a change in the future without closing your terminal and
  starting a new one.

    `exec bash`

  *Go forth and dominate!* No one is ever done learning the terminal.
  That's what makes it so great. There's always more to learn. But rest
  assured, once you master even the basics you'll be 10x faster than your
  GUI peers.
EOM

echo

