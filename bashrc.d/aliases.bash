
############################## All Aliases ##############################

# Keep in mind that aliases *cannot* be used from anything but the
# currently active shell (not subprocesses or any system exec() calls).
# Generally exported functions should be used instead (as recommended by
# the bash man page).

alias pie='perl -p -i -e '
alias grep='grep -i --colour=always'
alias egrep='egrep -i --colour=always'
alias fgrep='fgrep -i --colour=always'
alias kali='ssh kali'
alias pico='ssh pico'
alias ssh-keygen="ssh-keygen -t ed25519"
# TODO replace with eval $(twitch bashrc)

alias ignore="twitch chatoff"
alias unignore="twitch chaton"
alias focus="twitch focus"
alias flowstate="twitch flowstate"
alias casual="twitch casual"
alias casualstat="twitch casualstat"
alias chat="twitch chat"
alias chatnicks="twitch chatnicks"

alias raid="twitch raid"
alias c="clear"
alias x="exit"

