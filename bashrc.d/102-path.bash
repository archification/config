
export PATH=\
$HOME/bin:\
$HOME/bin/twitch:\
$HOME/go/bin:\
$HOME/.cargo/bin:\
$HOME/.node/bin:\
/usr/local/opt/coreutils/libexec/gnubin:\
/usr/local/go/bin:\
/usr/local/tinygo/bin:\
/usr/local/bin:\
/usr/local/sbin:\
/usr/games:\
/usr/sbin:\
/usr/bin:\
/snap/bin:\
/sbin:\
/bin

alias path='echo -e ${PATH//:/\\n}' # human readable path

