# Install Applications and Dependencies

Linux, Mac, Windows, and Raspberry Pi all need to have a few things installed and tweaked to enable an *Easy Bash Terminal Master* configuration. For example, only a few have `vim` installed by default (or even Bash 4+), and *none* of them have `lynx`, which is a damn shame because it is *still* one of the most useful tools ever created for all technologists.

See The [README from the `bashrc.d`](../bashrc.d/README.md) for naming conventions.

