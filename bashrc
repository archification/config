# Coded, curated, and configured by Rob Muhlestein (@robmuh)
# Rather than provied extensive comments find docs at
# <https://github.com/rwxrob/config/blob/master/README.md>
#
# Pilfer away. If you add or fix something let me know (if you want).
# You can override anything by adding a ~/.bash_{personal,private} if you
# prefer.
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>

# shell must be interactive
case $- in
  *i*) ;;
  *) return 
esac

# reset all aliases
unalias -a



[ -z "$OS" ] && export OS=`uname`
case "$OS" in
  Linux)          export PLATFORM=linux ;;
  *indows*)       export PLATFORM=windows ;;
  FreeBSD|Darwin) export PLATFORM=mac ;;
  *)              export PLATFORM=unknown ;;
esac

onmac () {
  [[ $PLATFORM == mac ]]  && return 0
  return 1
} && export -f onmac

onwin () {
  [[ $PLATFORM == windows ]]  && return 0
  return 1
} && export -f onwin

onlinux () {
  [[ $PLATFORM == linux ]]  && return 0
  return 1
} && export -f onlinux

onunknown () {
  [[ PLATFORM == unknown ]]  && return 0
  return 1
} && export -f onunknown


export PATH=\
$HOME/bin:\
$HOME/bin/twitch:\
$HOME/go/bin:\
$HOME/.cargo/bin:\
$HOME/.node/bin:\
/usr/local/opt/coreutils/libexec/gnubin:\
/usr/local/go/bin:\
/usr/local/tinygo/bin:\
/usr/local/bin:\
/usr/local/sbin:\
/usr/games:\
/usr/sbin:\
/usr/bin:\
/snap/bin:\
/sbin:\
/bin

alias path='echo -e ${PATH//:/\\n}' # human readable path


set bell-style none
#set -o noclobber

shopt -s checkwinsize
shopt -s expand_aliases
shopt -s nullglob
shopt -s globstar
shopt -s dotglob
shopt -s extglob

# There really is no reason to *not* use the GNU utilities for your login
# shell but there are lot of reasons *to* use it. The only time it could
# present a problem is using other shell scripts written specifically for
# Mac (darwin/bsd). There are far more scripts written to use the GNU
# utilities in the world. Plus you finger muscle memory will be consistent
# across Linux and Mac.

if [[ $PLATFORM = mac ]]; then
  if ! havecmd gls; then
    echo 'Need to `brew install coreutils` for Mac to work.'
  fi 
fi


# The tmp directory differs significantly between major operating systems
# so will use $TEMP throughout.

# TODO get the windows version (should be %TEMP%)

case "$PLATFORM" in
  windows) ;;
  *) export TEMP=/tmp
esac

mktempname () {
  echo "${1:-temp}.$$.$(tstamp)"
} && export -f mktempname

mktemppath () {
  [[ -z "$TEMP" ]] && return 1
  declare name=$(mktempname "$*")
  echo "$TEMP/$name"
} && export -f mktemppath

mktempdir () {
  declare path=$(mktemppath "$*")
  mkdir "$path"
  echo $path
} && export -f mktempdir

mktempfile () {
  declare path=$(mktemppath "$*")
  touch "$name"
  echo $name
} && export -f mktempfile
# Directory (cd) shortcuts. Using associative arrays pointing to
# environment variable names cuz names might not always match (otherwise
# would just ${foo^^} them). Might want to use a different env variable
# name at some point. In addtion to the lowercase name an identical copy
# of the alias is created with `cd` at the beginning to provide more
# flexibility when there is a private override (schedule might open the
# schedule instead of cd-ing into it while cdschedule always cds).

declare -A directories=(
  [repos]=REPOS
  [config]=CONFIG
  [personal]=PERSONAL
  [private]=PRIVATE
  [schedule]=SCHEDULE
  [blog]=BLOG
  [tokens]=TOKENS
  [downloads]=DOWNLOADS
  [desktop]=DESKTOP
  [pictures]=PICTURES
  [videos]=VIDEOS
  [images]=DISKIMAGES
  [vmachines]=VMACHINES
  [readme]=README
  [documents]=DOCUMENTS
)

for k in "${!directories[@]}"; do 
  v=${directories[$k]}
  alias $k='\
    if [[ -n "$'$v'" ]];\
      then cd "$'$v'";\
    else\
      warnln "\`\$'$v'\` not set. Consider adding to ~/.bash_{personal,private}.";\
    fi'
  # nice alternative when overriding others overriden
  alias cd$k='\
    if [[ -n "$'$v'" ]];\
      then cd "$'$v'";\
    else\
      warnln "\`\$'$v'\` not set. Consider adding to ~/.bash_{personal,private}.";\
    fi'
done

# Detect reasonable defaults (override in .bash_private). You'll want to
# set CONFIG in your PERSONAL or PRIVATE locations. See the following for
# examples of how to do this:
#
#    https://github.com/rwxrob/config-personal-sample
#    https://github.com/rwxrob/config-private-sample

declare -A defaults=(
  [DOWNLOADS]=~/Downloads
  [REPOS]=~/Repos
  [DESKTOP]=~/Desktop
  [DOCUMENTS]=~/Documents
  [README]=~/Documents/README   # README WorldPress content
  [PICTURES]=~/Pictures
  [VIDEOS]=~/Videos
  [DISKIMAGES]=~/DiskImages     # linux, arch, raspberrian, etc.
  [VMACHINES]=~/VMachines       # vmware, virtual box, etc.
  [TRASH]=~/Trash               # trash (see trash)
) 

for k in "${!defaults[@]}"; do
  v=${defaults[$k]}
  export $k=$v
done

# Graphic web site shortcuts.

declare -A sites=(
  [github]=github.com
  [gitlab]=gitlab.com
  [protonmail]=protonmail.com
  [skilstak]=skilstak.io
  [dockhub]=hub.docker.com
  [twitter]=twitter.com
  [medium]=medium.com
  [reddit]=reddit.com
  [patreon]=patreon.com
  [paypal]=paypal.com
  [hackerone]=hackerone.com
  [bugcrowd]=bugcrowd.com
  [synack]=synack.com
  [bls]=bls.gov
  [youtube]=youtube.com
  [vimeo]=vimeo.com
  [emojipedia]=emojipedia.com
  [netflix]=netflix.com
  [amazon]=amazon.com
)

for shortcut in "${!sites[@]}"; do 
  url=${sites[$shortcut]}
  alias $shortcut="open https://$url &>/dev/null"
done

alias '?'=duck
alias '??'=google

############################# Edit Shortcuts #############################

export EDITOR=vi
export VISUAL=vi
export EDITOR_PREFIX=vi

export VIMSPELL=(~/.vim/spell/*.add)
declare personalspell=(~/.vimpersonal/spell/*.add)
[[ -n "$personalspell" ]] && VIMSPELL=$personalspell
declare privatespell=(~/.vimprivate/spell/*.add)
[[ -n $privatespell ]] && VIMSPELL=$privatespell

# Commonly edited files.

declare -A edits=(
  [bashrc]=~/.bashrc
  [personal]=~/.bash_personal
  [private]=~/.bash_private
  [profile]=~/.profile
  [spell]=$VIMSPELL
)

for cmd in "${!edits[@]}"; do 
  path=${edits[$cmd]}
  case $PLATFORM in
    *) alias $EDITOR_PREFIX$cmd="$EDITOR '$path';warnln 'Make sure you git commit your changes (if needed).'" ;;
  esac
done
############################## Markdown-ish ##############################

# Minimal packrat parsing expression grammar (PEG):
# 
#   Mark      <- Span* !.
#   Span      <- Plain / StrongEm / Strong / Emphasis / Link / Code
#   StrongEm  <- '***' .* '***'
#   StrongEm  <- '***' .* '***'
#   Strong    <- '**' .* '**'
#   Emphasis  <- '*' .* '*'
#   Link      <- '<' .* '>'
#   Code      <- '`' .* '`'
#   Plain     <- DQUOTE / QUOTE / LARROW / RARROW / ELLIPSIS / .*
#   DQUOTE    <- '"'
#   QUOTE     <- "'"
#   LARROW    <- " <- " 
#   RARROW    <- " -> " 
#   ELLIPSIS  <- '...'
# 
# There's no character escape support because it isn't needed for most
# all use cases here. 

export md_strongem=$red     #  ***strongem***
export md_strong=$yellow    #  **strong**
export md_emphasis=$magen   #  *emphasis*
export md_code=$cyan        #  `code`
export md_plain=''          #  plain

mark () {
  declare inemphasis instrong instrongem incode indquote inquote inlink i
  declare defsol1="$normal"
  declare defsol2="$normalbg"

  # Detect default colors (fore, bg) from optional first and second
  # arguments. 

  issol "$1" && defsol1="$1" && shift
  issol "$1" && defsol2="$1" && shift
  declare defsol="$defsol1$defsol2"
  echo -n "$defsol"

  # The main content is almost always within single quotes to prevent
  # shell expansions, but we'll combine the remaining arguments anyway for
  # convenience (like echo does).

  declare buf=$*

  # recursive descent parser

  for (( i=0; i<${#buf}; i++ )); do 

    # ***strongem***

    if [[ "${buf:$i:3}" = '***' ]]; then
      if [[ -z "$instrongem" ]]; then
        echo -n "$md_strongem"
        #echo -n '<strongem>'
        instrongem=1
      else
        #echo -n '</strongem>'
        echo -n $defsol
        instrongem=''
      fi
      i=$[i+2]
      continue
    fi

    # **strong**

    if [[ "${buf:$i:2}" = '**' ]]; then
      if [[ -z "$instrong" ]]; then
        echo -n "$md_strong"
        #echo -n '<strong>'
        instrong=y
      else
        #echo -n '</strong>'
        echo -n $defsol
        instrong=''
      fi
      i=$[i+1]
      continue
    fi

    # *emphasis*

    if [[ "${buf:$i:1}" = '*' ]]; then
      if [[ -z "$inemphasis" ]]; then
        echo -n "$md_emphasis"
        #echo -n '<emphasis>'
        inemphasis=y
      else
        #echo -n '</emphasis>'
        echo -n $defsol
        inemphasis=''
      fi
      continue
    fi

    # `code`

    if [[ "${buf:$i:1}" = '`' ]]; then
      if [[ -z "$incode" ]]; then
        echo -n "$md_code"
        #echo -n '<code>'
        incode=y
      else
        #echo -n '</code>'
        echo -n $defsol
        incode=''
      fi
      continue
    fi

    # "

    if [[ "${buf:$i:1}" = '"' ]]; then
      if [[ -z "$indquote" ]]; then
        echo -n '“'
        indquote=y
      else
        echo -n '”'
        indquote=''
      fi
      continue
    fi

    # ' 

    if [[ "${buf:$i:1}" = "'" ]]; then
      if [[ -z "$inquote" ]]; then
        echo -n '‘'
        inquote=y
      else
        echo -n '’'
        inquote=''
      fi
      continue
    fi

    # <-

    if [[ "${buf:$i:4}" = ' <- ' ]]; then
      echo -n " ← "
      i=$[i+3]
      continue
    fi

    # ->

    if [[ "${buf:$i:4}" = ' -> ' ]]; then
      echo -n " → "
      i=$[i+3]
      continue
    fi

    # ...
    
    if [[ "${buf:$i:3}" = '...' ]]; then
      echo -n "…"
      i=$[i+2]
      continue
    fi

    # <link>

    if [[ "${buf:$i:1}" = '<' ]]; then
      if [[ -z "$inlink" ]]; then
        echo -n $md_code${buf:$i:1}
        #echo -n '<link>'
        inlink=y
      fi
      continue
    fi
    if [[ "${buf:$i:1}" = '>' ]]; then
      if [[ -n "$inlink" ]]; then
        #echo -n '</link>'
        echo -n ${buf:$i:1}$defsol
        inlink=''
      fi
      continue
    fi

    echo -n "${buf:$i:1}"
  done
  echo -n $reset
} && export -f mark

open () {
  # TODO improve the mime handling
  case $PLATFORM in
    mac) open $* ;;
    windows) telln 'Not yet supported.';;
    linux) xdg-open $*;;
  esac
} && export -f open

confirm () {
  declare yn
  read -p " [y/N] " yn
  [[ ${yn,,} =~ y(es)? ]] && return 0
  return 1
} && export -f confirm

tell () {
  declare buf=$(argsorin "$*")
  mark $md_plain "$buf"
} && export -f tell

telln () {
  tell "$*"; echo
} && export -f telln

remind () {
  declare buf=$(argsorin "$*")
  tell "*REMEMBER:* $buf"
} && export -f remind

remindln () {
  remind "$*"; echo
} && export -f remindln

danger () {
  declare buf=$(argsorin "$*")
  tell "${blinkon}***DANGER:***${blinkoff} $buf"
} && export -f danger

dangerln () {
  danger "$*"; echo
} && export -f dangerln

warn () {
  declare buf=$(argsorin "$*")
  tell "${yellow}WARNING:${reset} $buf"
} && export -f warn

warnln () {
  warn "$*"; echo
} && export -f warnln

usageln () {
  declare cmd="$1"; shift
  declare buf="$*"
  tell "**usage:** *$cmd* $buf"
  echo
} && export -f usageln
 
# When we must.


# TODO maybe later
#diff () {
  #diff --color=always $* | less
#} && export -f diff

# Recursively greps everything (except the stuff with -name). The first
# /dev/null is an old trick to force find to add the name/path to the
# results.
# TODO rewrite with kat and perg instead of the subshells


# Simple egrep-ish thing without the subshell and with Bash regular
# expressions.

perg () {
  declare exp="$1"; shift
  if [[ -z "$*" ]]; then
    while IFS= read -r line; do
      [[ $line =~ $exp ]] && echo "$line"
    done
    return
  fi
  for file in $*; do
    while IFS= read -r line; do
      [[ $line =~ $exp ]] && echo "$line"
    done < "$file"
  done
} && export -f perg

lower () {
  echo ${1,,}
} && export -f lower

upper () {
  echo ${1^^}
} && export -f upper

linecount () {
   IFS=$'\n'
   declare a=($1)
   echo ${#a[@]}
} && export -f linecount

# These are so much faster than basename and dirname.

basepart () {
  echo ${1##*/}
} && export -f basepart

dirpart () {
  echo ${1%/*}
} && export -f dirpart


# A friendlier cat that does not invoke a subshell.

kat () {
	declare line
  if [[ -z "$*" ]]; then
    while IFS= read -r line; do
      [[ $line =~ $exp ]] && echo "$line"
    done
    return
  fi
  for file in $*; do
    while IFS= read -r line; do
      echo "$line"
    done < "$file"
  done
} && export -f kat

# Same as kat but skips blank and lines with optional whitespace that
# begin with a comment character (#).

katlines () {
  for file in $*; do
    while IFS= read -r line; do
      [[ $line =~ ^\ *(#|$) ]] && continue
      echo "$line"
    done < "$file"
  done
} && export -f katlines


# True if anything that can be run from the command line exists, binaries,
# scripts, aliases, and functions.

havecmd () {
  type "$1" &>/dev/null
  return $?
} && export -f havecmd

# Moves an existing thing to the same path and name but with
# ".preserved.<tstamp>" at the end and echoes the new location. Usually
# preferable to destroying what was previously there. Can be used to roll
# back changes transactionally.

preserve () {
  declare target="${1%/}"
  [[ ! -e "$target" ]] && return 1
  declare new="$target.preserved.$(tstamp)"
  mv "$target" "$new" 
  echo "$new" 
} && export -f preserve

# Lists all the preserved files by matching the .preserved<tstamp> suffix.
# If passed an argument limits to only those preserved files matching that
# name (prefix).

lspreserved () {
  declare -a files
  if [[ -z "$1" ]]; then
      files=(*.preserved.[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])
  else
      files=("$1".preserved.[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])
  fi
  for i in "${files[@]}"; do
    echo "$i"
  done
} && export -f lspreserved

lastpreserved () {
  mapfile -t  a < <(lspreserved "$1")
  declare n=${#a[@]}
  echo "${a[n-1]}"
} && export -f lastpreserved

rmpreserved () {
  while IFS= read -r line; do
    rm -rf "$line"
  done < <(lspreserved)
} && export -f rmpreserved

# Undos the last preserve performed on the given target.

unpreserve () {
  declare last=$(lastpreserved "$*")
  [[ -z "$last" ]] && return 1
  mv "$last" "${last%.preserved*}"
} && export -f unpreserve

trash () {
  [[ -z "$TRASH" ]] && return 1
  mkdir -p "$TRASH"
  mv "$*" "$TRASH"
} && export -f trash

symlink () {
  declare from="$1"
  declare to="$2"
  if [[ -z "$from" || -z "$to" ]]; then
    usageln 'symlink <from> <to>'
    return 1
  fi
  preserve "$from"
  telln Linking $from'` -> `'$to 
  ln -fs $to $from                   # always hated that this is backwards
} && export -f symlink

stringscan () {
  declare buf="$1"
  declare handle=(echo)
  [[ -n "$2" ]] && handle=($2)
  for (( i=0; i<${#buf}; i++ )); do 
    ($handle ${buf:$i:1})
  done
} && export -f stringscan

firstline () {
  declare line
  declare IFS=
  read -r line <"$1"
  echo $line
} && export -f firstline

lastline () {
  # Tail is fastest because it does a seek to the end of file.
  tail -1 "$1"
} && export -f lastline

lastcmdline () {
  lastline "$HOME/.bash_history"
} && export -f lastcmdline

lastcmd () {
  declare cl=$(lastcmdline)
  echo ${cl%% *}
} && export -f lastcmd


############################### Date / Time ##############################

tstamp () {
  echo -n $1
  date '+%Y%m%d%H%M%S'
} && export -f tstamp

tstampfile () {
  declare path="$1"
  declare pre=${path%.*}
  declare suf=${path##*.}
  echo -n $pre.$(tstamp)
  [[  "$pre" != "$suf" ]] && echo .$suf
} && export -f tstampfile

now () {
  echo "$1" $(date "+%A, %B %e, %Y, %l:%M:%S%p")
} && export -f now

now-s () {
  echo "$1" $(date "+%A, %B %e, %Y, %l:%M %p")
} && export -f now-s

epoch () {
  date +%s
} && export -f now

watchnow () {
  declare -i delay="${1:-10}"
  havecmd setterm && setterm --cursor off
  trapterm 'setterm --cursor on; clear'
  while true; do 
    clear
    echo -n $(now-s) |lolcat
    now-s > ~/.now
    sleep $delay
  done
} && export -f watchnow

weekday () {
  echo $(lower $(date +"%A"))
} && export -f weekday

month () {
  echo $(lower $(date +"%B"))
} && export -f month

year () {
  echo $(lower $(date +"%Y"))
} && export -f year

week () {
  date +%W
} && export -f week

# Calls the compact ncal variation with Mondays first and including the
# week count during the year. I prefer keeping track of blogs and such by
# the week of the year and not more complicated months and dates. (No one
# ever plugs in a specific month and day into a long blog URL. They just
# want it to be short).

cal () {
  declare exe=$(which ncal)
  if [[ -z "$exe" ]]; then
    cal $* 
    return $?
  fi
  $exe -M -w $*
} && export -f cal

newest () {
  IFS=$'\n'
  f=($(ls -1 --color=never -trd ${1:-.}/*))
  echo "${f[-1]}"
} && export -f newest

lastdown () { echo "$(newest $DOWNLOADS)"; } && export -f lastdown
lastpic () { echo "$(newest $PICTURES)"; } && export -f lastpic
mvlast () { mv "$(lastdown)" "./$1"; } && export -f mvlast
mvlastpic () { mv "$(lastpic)" "./$1"; } && export -f mvlastpic

ex () {
  declare file=$1
  [[ -z "$file" ]] && usageln 'ex <compressed>' && return 1
  [[ ! -f "$file" ]] && tell 'Invalid file: `'$file'`' && return 1
  case $file in
    *.tar.bz2)   tar xjf $file;;
    *.tar.gz)    tar xzf $file;;
    *.bz2)       bunzip2 $file;;
    *.rar)       unrar x $file;;
    *.gz)        gunzip $file;;
    *.tar)       tar xf $file;;
    *.tbz2)      tar xjf $file;;
    *.tgz)       tar xzf $file;;
    *.zip)       unzip $file;;
    *.Z)         uncompress $file;;
    *.7z)        7z x $file;;
    *.xz)        unxz $file;;
    *)           tell 'Unknown suffix on file: `'$file'`'; return 1 ;;
  esac
}

eject () {
  declare path=/media/$USER
  declare first=$(ls -1 $path | head -1)
  declare mpoint=$path/$first
  [[ -z "$first" ]] && tell 'Nothing to eject.' && return
  umount $mpoint && tell "$first ejected" || tell 'Could not eject.'
} && export -f eject

usb () {
  declare path="/media/$USER"
  declare first=$(ls -1 "$path" | head -1)
  echo "$path/$first"
} && export -f usb

cdusb () {
  cd "$(usb)"
} && export -f cdusb

zeroblk () {
  [[ -z "$1" ]] && usageln 'zerobkd <blkdev>' && return 1
  [[ ! -b "/dev/$1" ]] && tell 'Not a block device.' && return 1
  danger 'Are you really sure you want to completly erase /dev/$1?'\
    || return 1
  sudo dd if=/dev/zero of=/dev/$1 bs=4M status=progress
  sync
} && export -f zeroblk


alias bat='\
  upower -i /org/freedesktop/UPower/devices/battery_BAT0 | \
  grep -E "state|to\ full|percentage"'
alias free='free -h'
alias df='df -h'
alias syserrors="sudo journalctl -p 3 -xb"
alias sysderrors="sudo systemctl --failed"
#alias top="gotop -c solarized -f -r .33 -m"
alias top="gotop -r .33"

listening () {
  case "$PLATFORM" in
    mac)                                 # TODO rewrite with perg 
      netstat -an -ptcp | grep LISTEN
      lsof -i -P | grep -i "listen"
      ;;
    *) netstat -tulpn |grep LISTEN ;;
  esac
} && export -f listening

sudolistening () {
  case "$PLATFORM" in
    mac)                                 # TODO rewrite with perg 
      sudo netstat -an -ptcp | grep LISTEN
      sudo lsof -i -P | grep -i "listen"
      ;;
    *) sudo netstat -tulpn |grep LISTEN ;;
  esac
} && export -f sudolistening

sshspeedtest () {
  yes | pv |ssh "$1" "cat >/dev/null"
} && export -f sshspeedtest

if havecmd dircolors; then
  if [ -r ~/.dircolors ]; then
    eval "$(dircolors -b ~/.dircolors)"
  else
    eval "$(dircolors -b)"
  fi
fi

alias lsplain='ls -h --color=never'
alias ls='ls -h --color=auto'
alias lx='ls -AlXB'    #  Sort by extension.
alias lxr='ls -ARlXB'  #  Sort by extension.
alias lk='ls -AlSr'    #  Sort by size, biggest last.
alias lkr='ls -ARlSr'  #  Sort by size, biggest last.
alias lt='ls -Altr'    #  Sort by date, most recent last.
alias ltr='ls -ARltr'  #  Sort by date, most recent last.
alias lc='ls -Altcr'   #  Sort by change time, most recent last.
alias lcr='ls -ARltcr' #  Sort by change time, most recent last.
alias lu='ls -Altur'   #  Sort by access time, most recent last.
alias lur='ls -ARltur' #  Sort by access time, most recent last.
alias ll='ls -Alhv'    #  A better long listing.
alias llr='ls -ARlhv'  #  Recursive long listing.
alias lr='ll -AR'      #  Recursive simple ls.
alias lm='ls |more'    #  Pipe through 'more'
alias lmr='lr |more'   #  Pipe through 'more'


################################## Curl ##################################

alias ipinfo="curl ipinfo.io"
alias weather="curl wttr.in"

# Igor Chubin is a god.

cheat() {
  where="$1"; shift
  IFS=+ curl "http://cht.sh/$where/ $*"
} && export -f cheat

############################ Web API / Tokens ############################

token () {
  [[ -z "$TOKENS" ]] && telln '`$TOKENS` is not set.' && return 1
  declare file="$TOKENS/$1"
  [[ ! -r "$file" ]] && return 1
  firstline "$TOKENS/$1"
} && export -f token

trapterm () {
  declare handler="$1"
  # TODO replace this with a pop/push alternative to preserve keep others
  trap "$handler; echo $'\b\b'; trap -- - SIGINT SIGTERM" SIGTERM SIGINT
} && export -f trapterm

############################### Completion ###############################

# Most completion is set near the function that uses it or internally inside
# the command itself using https://github.com/robmuh/cmdtab for completion.

if [[ -r /usr/share/bash-completion/bash_completion ]]; then
  . /usr/share/bash-completion/bash_completion
fi

if [ $PLATFORM == mac ]; then
  if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
  fi
fi

########################## Terminal ANSI Escapes #########################

alias fixstty='stty sane'

export escape=$'\033'
export reset=$'\033[0m'
export bold=$'\033[1m'
export underline=$'\033[4m'
export blinkon=$'\033[5m'
export blinkoff=$'\033[25m'
export inverse=$'\033[7m'
export inverseoff=$'\033[27m'
export normal=$'\033[39m'
export normalbg=$'\033[49m'

export clear=$'\033[H\033[2J'
clear () { echo -n $clear; } && export -f clear

# TODO lookup clearline (I have someplace in Go)

############################### ANSI Colors ##############################

ansicolors () {
for attr in 0 1 4 5 7 ; do
    echo "------------------------------------------------"
    printf "ESC[%s;Foreground;Background - \n" $attr
    for fore in 30 31 32 33 34 35 36 37; do
        for back in 40 41 42 43 44 45 46 47; do
            printf '\033[%s;%s;%sm %02s;%02s\033[0m' \
              $attr $fore $back $fore $back
        done
    printf '\n'
    done
done
} && export -f ansicolors

# foregrounds

_x=30; export black=$'\033['$_x'm'   # black
_r=31; export red=$'\033['$_r'm'     # red
_g=32; export green=$'\033['$_g'm'   # green
_y=33; export yellow=$'\033['$_y'm'  # yellow
_b=34; export blue=$'\033['$_b'm'    # blue
_m=35; export magenta=$'\033['$_m'm' # magenta
_c=36; export cyan=$'\033['$_c'm'    # cyan
_w=37; export white=$'\033['$_w'm'   # white

# backgrounds

_xb=40; export bgblack=$'\033['$_xb'm'   # bgblack
_rb=41; export bgred=$'\033['$_rb'm'     # bgred
_gb=42; export bggreen=$'\033['$_gb'm'   # bggreen
_yb=43; export bgyellow=$'\033['$_yb'm'  # bgyellow
_bb=44; export bgblue=$'\033['$_bb'm'    # bgblue
_mb=45; export bgmagenta=$'\033['$_mb'm' # bgmagenta
_cb=46; export bgcyan=$'\033['$_cb'm'    # bgcyan
_wb=47; export bgwhite=$'\033['$_wb'm'   # bgwhite

# modern bright (not bold) foregrounds

_X=90; export brblack=$'\033['$_X'm'   # brblack
_R=91; export brred=$'\033['$_R'm'     # brred
_G=92; export brgreen=$'\033['$_G'm'   # brgreen
_Y=93; export bryellow=$'\033['$_Y'm'  # bryellow
_B=94; export brblue=$'\033['$_B'm'    # brblue
_M=95; export brmagenta=$'\033['$_M'm' # brmagenta
_C=96; export brcyan=$'\033['$_C'm'    # brcyan
_W=97; export brwhite=$'\033['$_W'm'   # brwhite

# modern bright (not bold) backgrounds

_Xb=100 # brblack
_Rb=101 # brred
_Gb=102 # brgreen
_Yb=103 # bryellow
_Bb=104 # brblue
_Mb=105 # brmagenta
_Cb=106 # brcyan
_Wb=107 # brwhite

############################ Solarized Colors ############################

# https://ethanschoonover.com/solarized/

# Modern terminals no longer require Solarized to use bold (\033[1m) and
# can use the equivalent background numbers instead (40+). Unfortunately
# for backward compatibility this means that bold itself cannot be used
# dependably with Solarized because it also switches to the legacy color
# variation (for example, bold-red becomes bold-orange instead).
# (Personally I've abandonned Solarized for a more traditional approach
# with higher contrast -- particularly for visibility on small devices
# where I'm told people watch my stream (when I'm not playing around with
# retro terminals for fun.)

# (Yes I know about 256 and rgb color escapes, not interested, but if you
# *are* interested and want to use the entire CSS specification for every
# color ever named have a look at https://github.com/rwxrob/go-colors to
# get all the specific RGB values.)

export soly=$'\033['$_y'm'; export yellow=$soly
export solo=$'\033['$_R'm'; export orange=$solo
export solr=$'\033['$_r'm'; export red=$solr
export solm=$'\033['$_m'm'; export magen=$solm
export solv=$'\033['$_M'm'; export violet=$solv
export solb=$'\033['$_b'm'; export blue=$solb
export solc=$'\033['$_c'm'; export cyan=$solc
export solg=$'\033['$_g'm'; export green=$solg

export solb03=$'\033['$_X'm'; export base03=$solb03
export solb02=$'\033['$_x'm'; export base02=$solb02
export solb01=$'\033['$_G'm'; export base01=$solb01
export solb00=$'\033['$_Y'm'; export base00=$solb00
export solb0=$'\033['$_B'm';  export base0=$solb0
export solb1=$'\033['$_C'm';  export base1=$solb1
export solb2=$'\033['$_w'm';  export base2=$solb2
export solb3=$'\033['$_W'm';  export base3=$solb3
 
export solY=$'\033['$_yb'm'; export bgyellow=$solY
export solO=$'\033['$_Rb'm'; export bgorange=$solO
export solR=$'\033['$_rb'm'; export bgred=$solR
export solM=$'\033['$_mb'm'; export bgmagen=$solM
export solV=$'\033['$_Mb'm'; export bgviolet=$solV
export solB=$'\033['$_bb'm'; export bgblue=$solB
export solC=$'\033['$_cb'm'; export bgcyan=$solC
export solG=$'\033['$_gb'm'; export bggreen=$solG

export solB03=$'\033['$_Xb'm'; export bgbase03=$solB03
export solB02=$'\033['$_xb'm'; export bgbase02=$solB02
export solB01=$'\033['$_Gb'm'; export bgbase01=$solB01
export solB00=$'\033['$_Yb'm'; export bgbase00=$solB00
export solB0=$'\033['$_Bb'm';  export bgbase0=$solB0
export solB1=$'\033['$_Cb'm';  export bgbase1=$solB1
export solB2=$'\033['$_wb'm';  export bgbase2=$solB2
export solB3=$'\033['$_Wb'm';  export bgbase3=$solB3

solcolors=(y o r m v b c g)
solbases=(b03 b02 b01 b00 b0 b1 b2 b3)

# TODO add a decolor (port from Go version)

rnd () {
  declare num=$[RANDOM%8]
  declare col=${solcolors[$num]}
  declare name=sol$col
  echo -n ${!name}
} && export -f rnd

iscolor () {
  case "$1" in
    $yellow|$orange|$red|$magen|$violet|$blue|$cyan|$green)
      return 0
      ;;
  esac
  return 1
} && export -f iscolor

isbgcolor () {
  case "$1" in
    $bgyellow|$bgorange|$bgred|$bgmagen|$bgviolet|$bgblue|$bgcyan|$bggreen)
      return 0
      ;;
  esac
  return 1
} && export -f isbgcolor

isbase () {
  case "$1" in
    $base03|$base02|$base01|$base00|$base0|$base1|$base2|$base3)
      return 0
      ;;
  esac
  return 1
} && export -f isbase

isbgbase () {
  case "$1" in
    $bgbase03|$bgbase02|$bgbase01|$bgbase00|$bgbase0|$bgbase1|$bgbase2|$bgbase3)
      return 0
      ;;
  esac
  return 1
} && export -f isbgbase

issol () {
  iscolor $1 && return 0
  isbase $1 && return 0
  isbgcolor $1 && return 0
  isbgbase $1 && return 0
  return 1
} && export -f issol

# ooooooo, pretty

sols () {
  printf "     "
  for fore in ${solbases[@]} ${solcolors[@]} ;do
    declare c=sol$fore
    printf "%-4s" $fore
  done
  printf $reset
  for back in ${solbases[@]} ${solcolors[@]};do
    declare b=sol${back^^}
    printf "$reset\n %-4s%s" ${back^^} ${!b} 
    for fore in ${solbases[@]} ${solcolors[@]};do
      declare f=sol${fore}
      printf "%s%s%-4s" ${!b} ${!f} sol
    done
  done
  printf $reset
  printf "\n[reset clear]\n"
} && export -f sols

############################### Modern RGB ###############################

rgb () {
  echo -n $'\033[38;2;'$1';'$2';'$3'm'
} && export -f rgb

rgbg () {
  echo -n $'\033[48;2;'$1';'$2';'$3'm'
} && export -f rgb

############################## Named Colors ##############################

export twitch=$'\033[38;2;169;112;255m'
export twrwxrob=$'\033[38;2;230;77;73m'

########################### CSS Standard Colors ##########################

# Thanks to the wonderful work of Ethan Baker (github.com/ethanbaker).

export aliceblue=$'\033[38;2;240;248;255m'
export antiquewhite=$'\033[38;2;250;235;215m'
export aqua=$'\033[38;2;0;255;255m'
export aquamarine=$'\033[38;2;127;255;212m'
export azure=$'\033[38;2;1240;255;255m'
export beige=$'\033[38;2;245;245;220m'
export bisque=$'\033[38;2;255;228;196m'
export black=$'\033[38;2;0;0;0m'
export blanchedalmond=$'\033[38;2;255;235;205m'
export blue=$'\033[38;2;0;0;255m'
export blueviolet=$'\033[38;2;138;43;226m'
export brown=$'\033[38;2;165;42;42m'
export burlywood=$'\033[38;2;222;184;135m'
export cadetblue=$'\033[38;2;95;158;160m'
export chartreuse=$'\033[38;2;95;158;160m'
export chocolate=$'\033[38;2;210;105;30m'
export coral=$'\033[38;2;255;127;80m'
export cornflowerblue=$'\033[38;2;100;149;237m'
export cornsilk=$'\033[38;2;255;248;220m'
export crimson=$'\033[38;2;220;20;60m'
export cyan=$'\033[38;2;0;255;255m'
export darkblue=$'\033[38;2;0;0;139m'
export darkcyan=$'\033[38;2;0;139;139m'
export darkgoldenrod=$'\033[38;2;184;134;11m'
export darkgray=$'\033[38;2;169;169;169m'
export darkgreen=$'\033[38;2;0;100;0m'
export darkkhaki=$'\033[38;2;189;183;107m'
export darkmagenta=$'\033[38;2;139;0;139m'
export darkolivegreen=$'\033[38;2;85;107;47m'
export darkorange=$'\033[38;2;255;140;0m'
export darkorchid=$'\033[38;2;153;50;204m'
export darkred=$'\033[38;2;139;0;0m'
export darksalmon=$'\033[38;2;233;150;122m'
export darkseagreen=$'\033[38;2;143;188;143m'
export darkslateblue=$'\033[38;2;72;61;139m'
export darkslategray=$'\033[38;2;47;79;79m'
export darkturquoise=$'\033[38;2;0;206;209m'
export darkviolet=$'\033[38;2;148;0;211m'
export deeppink=$'\033[38;2;255;20;147m'
export deepskyblue=$'\033[38;2;0;191;255m'
export dimgray=$'\033[38;2;0;191;255m'
export dodgerblue=$'\033[38;2;30;144;255m'
export firebrick=$'\033[38;2;178;34;34m'
export floralwhite=$'\033[38;2;255;250;240m'
export forestgreen=$'\033[38;2;34;139;34m'
export fuchsia=$'\033[38;2;255;0;255m'
export gainsboro=$'\033[38;2;220;220;220m'
export ghostwhite=$'\033[38;2;248;248;255m'
export gold=$'\033[38;2;255;215;0m'
export goldenrod=$'\033[38;2;218;165;32m'
export gray=$'\033[38;2;127;127;127m'
export green=$'\033[38;2;0;128;0m'
export greenyellow=$'\033[38;2;173;255;47m'
export honeydew=$'\033[38;2;240;255;240m'
export hotpink=$'\033[38;2;255;105;180m'
export indianred=$'\033[38;2;205;92;92m'
export indigo=$'\033[38;2;75;0;130m'
export ivory=$'\033[38;2;255;255;240m'
export khaki=$'\033[38;2;240;230;140m'
export lavender=$'\033[38;2;230;230;250m'
export lavenderblush=$'\033[38;2;255;240;245m'
export lawngreen=$'\033[38;2;124;252;0m'
export lemonchiffon=$'\033[38;2;255;250;205m'
export lightblue=$'\033[38;2;173;216;230m'
export lightcoral=$'\033[38;2;240;128;128m'
export lightcyan=$'\033[38;2;224;255;255m'
export lightgoldenrodyellow=$'\033[38;2;250;250;210m'
export lightgreen=$'\033[38;2;144;238;144m'
export lightgrey=$'\033[38;2;211;211;211m'
export lightpink=$'\033[38;2;255;182;193m'
export lightsalmon=$'\033[38;2;255;160;122m'
export lightseagreen=$'\033[38;2;32;178;170m'
export lightskyblue=$'\033[38;2;135;206;250m'
export lightslategray=$'\033[38;2;119;136;153m'
export lightsteelblue=$'\033[38;2;176;196;222m'
export lightyellow=$'\033[38;2;255;255;224m'
export lime=$'\033[38;2;0;255;0m'
export limegreen=$'\033[38;2;50;205;50m'
export linen=$'\033[38;2;250;240;230m'
export magenta=$'\033[38;2;255;0;255m'
export maroon=$'\033[38;2;128;0;0m'
export mediumaquamarine=$'\033[38;2;102;205;170m'
export mediumblue=$'\033[38;2;0;0;205m'
export mediumorchid=$'\033[38;2;186;85;211m'
export mediumpurple=$'\033[38;2;147;112;219m'
export mediumseagreen=$'\033[38;2;60;179;113m'
export mediumslateblue=$'\033[38;2;123;104;238m'
export mediumspringgreen=$'\033[38;2;0;250;154m'
export mediumturquoise=$'\033[38;2;72;209;204m'
export mediumvioletred=$'\033[38;2;199;21;133m'
export midnightblue=$'\033[38;2;25;25;112m'
export mintcream=$'\033[38;2;245;255;250m'
export mistyrose=$'\033[38;2;255;228;225m'
export moccasin=$'\033[38;2;255;228;181m'
export navajowhite=$'\033[38;2;255;222;173m'
export navy=$'\033[38;2;0;0;128m'
export navyblue=$'\033[38;2;159;175;223m'
export oldlace=$'\033[38;2;253;245;230m'
export olive=$'\033[38;2;128;128;0m'
export olivedrab=$'\033[38;2;107;142;35m'
export orange=$'\033[38;2;255;165;0m'
export orangered=$'\033[38;2;255;69;0m'
export orchid=$'\033[38;2;218;112;214m'
export palegoldenrod=$'\033[38;2;238;232;170m'
export palegreen=$'\033[38;2;152;251;152m'
export paleturquoise=$'\033[38;2;175;238;238m'
export palevioletred=$'\033[38;2;219;112;147m'
export papayawhip=$'\033[38;2;255;239;213m'
export peachpuff=$'\033[38;2;255;218;185m'
export peru=$'\033[38;2;205;133;63m'
export pink=$'\033[38;2;255;192;203m'
export plum=$'\033[38;2;221;160;221m'
export powderblue=$'\033[38;2;176;224;230m'
export purple=$'\033[38;2;128;0;128m'
export red=$'\033[38;2;255;0;0m'
export rosybrown=$'\033[38;2;188;143;143m'
export royalblue=$'\033[38;2;65;105;225m'
export saddlebrown=$'\033[38;2;139;69;19m'
export salmon=$'\033[38;2;250;128;114m'
export sandybrown=$'\033[38;2;244;164;96m'
export seagreen=$'\033[38;2;46;139;87m'
export seashell=$'\033[38;2;255;245;238m'
export sienna=$'\033[38;2;160;82;45m'
export silver=$'\033[38;2;192;192;192m'
export skyblue=$'\033[38;2;135;206;235m'
export slateblue=$'\033[38;2;106;90;205m'
export slategray=$'\033[38;2;112;128;144m'
export snow=$'\033[38;2;255;250;250m'
export springgreen=$'\033[38;2;0;255;127m'
export steelblue=$'\033[38;2;70;130;180m'
export tan=$'\033[38;2;210;180;140m'
export teal=$'\033[38;2;0;128;128m'
export thistle=$'\033[38;2;216;191;216m'
export tomato=$'\033[38;2;255;99;71m'
export turquoise=$'\033[38;2;64;224;208m'
export violet=$'\033[38;2;238;130;238m'
export wheat=$'\033[38;2;245;222;179m'
export white=$'\033[38;2;255;255;255m'
export whitesmoke=$'\033[38;2;245;245;245m'
export yellow=$'\033[38;2;255;255;0m'
export yellowgreen=$'\033[38;2;139;205;50m'

export alicebluebg=$'\033[48;2;240;248;255m'
export antiquewhitebg=$'\033[48;2;250;235;215m'
export aquabg=$'\033[48;2;0;255;255m'
export aquamarinebg=$'\033[48;2;127;255;212m'
export azurebg=$'\033[48;2;1240;255;255m'
export beigebg=$'\033[48;2;245;245;220m'
export bisquebg=$'\033[48;2;255;228;196m'
export blackbg=$'\033[48;2;0;0;0m'
export blanchedalmondbg=$'\033[48;2;255;235;205m'
export bluebg=$'\033[48;2;0;0;255m'
export bluevioletbg=$'\033[48;2;138;43;226m'
export brownbg=$'\033[48;2;165;42;42m'
export burlywoodbg=$'\033[48;2;222;184;135m'
export cadetbluebg=$'\033[48;2;95;158;160m'
export chartreusebg=$'\033[48;2;95;158;160m'
export chocolatebg=$'\033[48;2;210;105;30m'
export coralbg=$'\033[48;2;255;127;80m'
export cornflowerbluebg=$'\033[48;2;100;149;237m'
export cornsilkbg=$'\033[48;2;255;248;220m'
export crimsonbg=$'\033[48;2;220;20;60m'
export cyanbg=$'\033[48;2;0;255;255m'
export darkbluebg=$'\033[48;2;0;0;139m'
export darkcyanbg=$'\033[48;2;0;139;139m'
export darkgoldenrodbg=$'\033[48;2;184;134;11m'
export darkgraybg=$'\033[48;2;169;169;169m'
export darkgreenbg=$'\033[48;2;0;100;0m'
export darkkhakibg=$'\033[48;2;189;183;107m'
export darkmagentabg=$'\033[48;2;139;0;139m'
export darkolivegreenbg=$'\033[48;2;85;107;47m'
export darkorangebg=$'\033[48;2;255;140;0m'
export darkorchidbg=$'\033[48;2;153;50;204m'
export darkredbg=$'\033[48;2;139;0;0m'
export darksalmonbg=$'\033[48;2;233;150;122m'
export darkseagreenbg=$'\033[48;2;143;188;143m'
export darkslatebluebg=$'\033[48;2;72;61;139m'
export darkslategraybg=$'\033[48;2;47;79;79m'
export darkturquoisebg=$'\033[48;2;0;206;209m'
export darkvioletbg=$'\033[48;2;148;0;211m'
export deeppinkbg=$'\033[48;2;255;20;147m'
export deepskybluebg=$'\033[48;2;0;191;255m'
export dimgraybg=$'\033[48;2;0;191;255m'
export dodgerbluebg=$'\033[48;2;30;144;255m'
export firebrickbg=$'\033[48;2;178;34;34m'
export floralwhitebg=$'\033[48;2;255;250;240m'
export forestgreenbg=$'\033[48;2;34;139;34m'
export fuchsiabg=$'\033[48;2;255;0;255m'
export gainsborobg=$'\033[48;2;220;220;220m'
export ghostwhitebg=$'\033[48;2;248;248;255m'
export goldbg=$'\033[48;2;255;215;0m'
export goldenrodbg=$'\033[48;2;218;165;32m'
export graybg=$'\033[48;2;127;127;127m'
export greenbg=$'\033[48;2;0;128;0m'
export greenyellowbg=$'\033[48;2;173;255;47m'
export honeydewbg=$'\033[48;2;240;255;240m'
export hotpinkbg=$'\033[48;2;255;105;180m'
export indianredbg=$'\033[48;2;205;92;92m'
export indigobg=$'\033[48;2;75;0;130m'
export ivorybg=$'\033[48;2;255;255;240m'
export khakibg=$'\033[48;2;240;230;140m'
export lavenderbg=$'\033[48;2;230;230;250m'
export lavenderblushbg=$'\033[48;2;255;240;245m'
export lawngreenbg=$'\033[48;2;124;252;0m'
export lemonchiffonbg=$'\033[48;2;255;250;205m'
export lightbluebg=$'\033[48;2;173;216;230m'
export lightcoralbg=$'\033[48;2;240;128;128m'
export lightcyanbg=$'\033[48;2;224;255;255m'
export lightgoldenrodyellowbg=$'\033[48;2;250;250;210m'
export lightgreenbg=$'\033[48;2;144;238;144m'
export lightgreybg=$'\033[48;2;211;211;211m'
export lightpinkbg=$'\033[48;2;255;182;193m'
export lightsalmonbg=$'\033[48;2;255;160;122m'
export lightseagreenbg=$'\033[48;2;32;178;170m'
export lightskybluebg=$'\033[48;2;135;206;250m'
export lightslategraybg=$'\033[48;2;119;136;153m'
export lightsteelbluebg=$'\033[48;2;176;196;222m'
export lightyellowbg=$'\033[48;2;255;255;224m'
export limebg=$'\033[48;2;0;255;0m'
export limegreenbg=$'\033[48;2;50;205;50m'
export linenbg=$'\033[48;2;250;240;230m'
export magentabg=$'\033[48;2;255;0;255m'
export maroonbg=$'\033[48;2;128;0;0m'
export mediumaquamarinebg=$'\033[48;2;102;205;170m'
export mediumbluebg=$'\033[48;2;0;0;205m'
export mediumorchidbg=$'\033[48;2;186;85;211m'
export mediumpurplebg=$'\033[48;2;147;112;219m'
export mediumseagreenbg=$'\033[48;2;60;179;113m'
export mediumslatebluebg=$'\033[48;2;123;104;238m'
export mediumspringgreenbg=$'\033[48;2;0;250;154m'
export mediumturquoisebg=$'\033[48;2;72;209;204m'
export mediumvioletredbg=$'\033[48;2;199;21;133m'
export midnightbluebg=$'\033[48;2;25;25;112m'
export mintcreambg=$'\033[48;2;245;255;250m'
export mistyrosebg=$'\033[48;2;255;228;225m'
export moccasinbg=$'\033[48;2;255;228;181m'
export navajowhitebg=$'\033[48;2;255;222;173m'
export navybg=$'\033[48;2;0;0;128m'
export navybluebg=$'\033[48;2;159;175;223m'
export oldlacebg=$'\033[48;2;253;245;230m'
export olivebg=$'\033[48;2;128;128;0m'
export olivedrabbg=$'\033[48;2;107;142;35m'
export orangebg=$'\033[48;2;255;165;0m'
export orangeredbg=$'\033[48;2;255;69;0m'
export orchidbg=$'\033[48;2;218;112;214m'
export palegoldenrodbg=$'\033[48;2;238;232;170m'
export palegreenbg=$'\033[48;2;152;251;152m'
export paleturquoisebg=$'\033[48;2;175;238;238m'
export palevioletredbg=$'\033[48;2;219;112;147m'
export papayawhipbg=$'\033[48;2;255;239;213m'
export peachpuffbg=$'\033[48;2;255;218;185m'
export perubg=$'\033[48;2;205;133;63m'
export pinkbg=$'\033[48;2;255;192;203m'
export plumbg=$'\033[48;2;221;160;221m'
export powderbluebg=$'\033[48;2;176;224;230m'
export purplebg=$'\033[48;2;128;0;128m'
export redbg=$'\033[48;2;255;0;0m'
export rosybrownbg=$'\033[48;2;188;143;143m'
export royalbluebg=$'\033[48;2;65;105;225m'
export saddlebrownbg=$'\033[48;2;139;69;19m'
export salmonbg=$'\033[48;2;250;128;114m'
export sandybrownbg=$'\033[48;2;244;164;96m'
export seagreenbg=$'\033[48;2;46;139;87m'
export seashellbg=$'\033[48;2;255;245;238m'
export siennabg=$'\033[48;2;160;82;45m'
export silverbg=$'\033[48;2;192;192;192m'
export skybluebg=$'\033[48;2;135;206;235m'
export slatebluebg=$'\033[48;2;106;90;205m'
export slategraybg=$'\033[48;2;112;128;144m'
export snowbg=$'\033[48;2;255;250;250m'
export springgreenbg=$'\033[48;2;0;255;127m'
export steelbluebg=$'\033[48;2;70;130;180m'
export tanbg=$'\033[48;2;210;180;140m'
export tealbg=$'\033[48;2;0;128;128m'
export thistlebg=$'\033[48;2;216;191;216m'
export tomatobg=$'\033[48;2;255;99;71m'
export turquoisebg=$'\033[48;2;64;224;208m'
export violetbg=$'\033[48;2;238;130;238m'
export wheatbg=$'\033[48;2;245;222;179m'
export whitebg=$'\033[48;2;255;255;255m'
export whitesmokebg=$'\033[48;2;245;245;245m'
export yellowbg=$'\033[48;2;255;255;0m'
export yellowgreenbg=$'\033[48;2;139;205;50m'

# Not a fan of ridiculously distracting command prompts. Prompt should
# fade into background, not demand focus. The printf magic ensures that
# any output that does not end with a newline will not put a prompt
# immediately after it. Never forget the \[$color\] brackets that escape
# the width of the color escapes so they don't affect line wrapping.

# See https://github.com/ryanoasis/powerline-extra-symbols

export glypharrow=$'\ue0b0'
export glyphflames=$'\ue0c0'
export glyphrounded=$'\ue0b4'
export glyphbits=$'\ue0c6'
export glyphhex=$'\ue0cc'

export promptglyph=$glypharrow
export promptbgr=40
export promptbgg=40
export promptbgb=40
export promptuserc=$twrwxrob
export prompthostc=$twitch
export promptdirc=$bold$base04

# TODO detect current home system and only show home of main system so
# that when used on remote systems the name is displayed.

export PROMPT_COMMAND='
  touch "$HOME/.bash_history"
  promptbg=$(rgbg $promptbgr $promptbgg $promptbgb)
  # FIXME 
  promptglyphc=$(rgb $promptbgr $promptbgg $promptbgb)
  if [[ $HOME == $PWD ]]; then
    export PROMPTDIR="🏡"
  elif [[ $HOME == ${PWD%/*} ]]; then
    export PROMPTDIR="/${PWD##*/}"
  elif [[ / == $PWD ]]; then
    export PROMPTDIR="/"
  elif [[ "" == ${PWD%/*} ]]; then
    export PROMPTDIR=$PWD
  else
    # TODO fixeme with ascii char
    export PROMPTDIR=…/${PWD##*/}
  fi
  if [[ $EUID == 0 ]]; then
    PS1="\[$blinkon$promptbg$red\]\u\[$base03$blinkoff\]@\[\$prompthostc\]\h \[$base03\]\[$promptdirc\]"$PROMPTDIR" \[$reset$promptglyphc\]$promptglyph \[$reset\]"
  else 
    PS1="\[$base03\]\[$promptdirc\]"$PROMPTDIR" \[$reset$twitch\]$promptglyph \[$reset\]"
 fi
'
############################# Editor / Pager #############################

# The first and only visual editor that matters. It's on everything. Stop
# complaining and apologizing for it and learn the fucking thing. Then vi
# all the things and make sure to not fuck up your vi muscle memory with
# arrows and shit you cannot use with an old vi you might be forced to use
# someday. There's really no reason. Learn home row and CTRL-[ for escape.

havecmd vim && alias vi=vim

[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
alias more='less -R'

# Here's your colored man pages right here.

export LESS_TERMCAP_mb=$magen
export LESS_TERMCAP_md=$yellow
export LESS_TERMCAP_me=$reset
export LESS_TERMCAP_se=$reset
export LESS_TERMCAP_so=$blue
export LESS_TERMCAP_ue=$reset
export LESS_TERMCAP_us=$violet


###################### Gnome Terminal Configuration ######################

# Gnome terminal configuration is never easy to save. These are the
# documented ways to save, reset, and restore a gnome-configuration
# setting for an individual user. The commands are easy enough to execute
# so these are included mostly as reminders about how to do it.

gnome-terminal-conf-dump () {
  if ! havecmd dconf ; then
    telln 'Cannot locate: `dconf`'
    return 1
  fi
  declare dest="${1:-/tmp/dconf-terminal-$(tstamp)}"
  declare buf=$(dconf dump /org/gnome/terminal/)
  echo "$buf" >| $dest
  echo $dest
} && export -f gnome-terminal-conf-dump

gnome-terminal-conf-reset () {
  if ! havecmd dconf ; then
    telln 'Cannot locate: `dconf`'
    return 1
  fi
  dconf reset -f /org/gnome/terminal/
} && export -f gnome-terminal-conf-reset

gnome-terminal-conf-load () {
  if ! havecmd dconf ; then
    telln 'Cannot locate: `dconf`'
    return 1
  fi
  if [[ -z "$1" ]]; then
    usageln 'gnome-terminal-conf-load <path>'
    return 1
  fi
  dconf load < "$1"
} && export -f gnome-terminal-conf-load

termtitle () {
  printf "\033]0;$*\007"
} && export -f termtitle

alias tmatrix="tmatrix -s 15 --fade"

hidecursor () {
  if havecmd setterm; then
    setterm --cursor off
    trapterm 'setterm --cursor on; clear'
  fi
} && export -f hidecursor
############################## Notifications #############################

# Notify sends a normal notification using notify-send. Use notify-send
# for more specific notifications.

notify () {
  notify-send "$*"
} && export -f notify

################################# History ################################

# This history configuration assume a reasonably conservative system on
# which saving the session history after the session is fine because
# sessions are very frequently started and stopped due to extensive TMUX
# pane usage. Some may prefer to add a `history -a; history -c; history
# -r` to their PROMPT_COMMAND if they tend to stay in the same session for
# a long time and want the history to be updated for all to see.

shopt -s histappend
HISTCONTROL=ignoreboth
HISTSIZE=5000
HISTFILESIZE=10000

################################ ASCII Art ###############################

doh () {
  echo -n $base3
  kat <<'EOM'

                 _ ,___,-'",-=-.           
       __,-- _ _,-'_)_  (""`'-._\ `.  [31m _____   ____  _    _ _  [0m [97m
    _,'  __ |,' ,-' __)  ,-     /. |  [31m|  __ \ / __ \| |  | | |[0m [97m
  ,'_,--'   |     -'  _)/         `\  [31m| |  | | |  | | |__| | |[0m [97m
,','      ,'       ,-'_,`           : [31m| |  | | |  | |  __  | |[0m [97m
,'     ,-'       ,(,-(              : [31m| |__| | |__| | |  | |_|[0m [97m
     ,'       ,-' ,    _            ; [31m|_____/ \____/|_|  |_(_)[0m [97m
    /        ,-._/`---'            /                                
   /        (____)(----. )       ,'                                 
  /         (      `.__,     /\ /,         
 :           ;-.___         /__\/|         
 |         ,'      `--.      -,\ |         
 :        /            \    .__/           
  \      (__            \    |_            
   \       ,`-, *       /   _|,\           
    \    ,'   `-.     ,'_,-'    \          
   (_\,-'    ,'\")--,'-'       __\         
    \       /  // ,'|      ,--'  `-.       
     `-.    `-/ \'  |   _,'         `.     
        `-._ /      `--'/             \    
          ,'           /               \    
          /            |                \  
 [92m-hrr-[97m ,-'             |                /  
      /                               -'   

EOM
  echo -n $reset
} && export -f doh

humm () {
  echo -n $base3
  kat <<'EOM'
       ,---.                        
     ,.'-.   \                      
    ( ( ,'"""""-.                   
    `,X          `.        [33m  _____                   [0m[97m
    /` `           `._     [33m |  |  |_ _ _____ _____   [0m[97m
   (            ,   ,_\    [33m |     | | |     |     |_ [0m[97m
   |          ,---.,'o `.  [33m |__|__|___|_|_|_|_|_|_|_| [0m[97m
   |         / o   \     )          
    \ ,.    (      .____,           
     \| \    \____,'     \          
   '`'\  \        _,____,'          
   \  ,--      ,-'     \            
     ( C     ,'         \           
      `--'  .'           |          
        |   |            |          
      __|    \        ,-'_          
     / `L     `._  _,'  ' `.        
    /    `--.._  `',.   _\  `       
    `-.       /\  | `. ( ,\  \      
   _/  `-._  /  \ |--'  (     \     
 -'  `-.   `'    \/\`.   `.    )    
        \  [92m-hrr-[97m    \ `.  |    |    

EOM
} && export -f humm

##########################################################################

# Prints some asciiart by key name from the exported asciiart associative
# array. If not found says so on stdout and returns 1.

ascii () {
  declare name="${1:-hummm}"
  if [[ -v $asciiart[$name] ]]; then
    telln 'Sorry no ascii art found for `'$name'`'
    return 1
  fi
  declare text=${asciiart[$name]}
  echo -n "${text:1}"
} && export -f ascii

# TODO I *really* need an asciicenter function that detects and
# automatically centers. It's simple enough, I just need to get it done.
# Could have finished already in the time I spend adding spaces for
# padding to the art.

# Joins two files of text (presumably ascii art). You should add adequate
# spaces to the left file so that they match. (Might calculate that
# someday but this is fine for now.)

asciijoin () {
  declare -a a b
  declare i
  mapfile -t  a < "$1"
  mapfile -t  b < "$2"
  declare -i arows=${#a[@]}
  declare -i brows=${#b[@]}
  declare -i rows
  if [[ $arows > $brows ]]; then # ???
    rows=$brows
  else
    rows=$arows
  fi
  #declare -i awidth
  for (( i=0; i<$rows; i++ )); do 
    declare aline=${a[$i]}
    declare bline=${b[$i]}
    declare -i alen=${#aline}
    #[[ $alen > $awidth ]] && awidth=$alen
    echo "$aline $bline"
  done
  # FIXME second pass with left padding in case image on the left has
  # fewer lines than the one on the right
} && export -f asciijoin

alias cmatrix="cmatrix -b -C red -u 6"

watchdragon () {
  hidecursor
  while true; do
    clear
    echo 
    fortune -s | cowsay -f dragon |lolcat
    sleep 120
  done
} && export -f watchdragon

########################## System Administration #########################

is-valid-username () {
  [[ "$1" =~ ^[a-z_][a-z0-9_]{0,31}$ ]] && return 0
  return 1
} && export -f is-valid-username

#change-user-name () {
#  declare old="$1"
#  declare new="$2"
#  if [[ -z "$old" || -z "$new" ]]; then
#    usageln 'change-user-name <old> <new>'
#    return 1
#  fi
#  ! is-valid-username "$old" && telln 'Invalid old username: `'$old'`' && return 1
#  ! is-valid-username "$new" && telln 'Invalid new username: `'$new'`' && return 1
#  sudo groupadd $new
#  # TODO rename the directory, test on vm
#  sudo usermod -d /home/$new -m -g $new -l $new $old
#}

preview () {
  browser-sync start \
    --no-notify --no-ui \
    --ignore '**/.*' \
    -sw
} && export -f preview

httphead () {
  curl -I -L "$@"
} && export -f httphead

################################ Encoding ################################

rot13 () {
  declare data=$(argsorin "$*")
  echo "$data" | tr '[A-Za-z]' '[N-ZA-Mn-za-m]'
} && export -f rot13


####################### Git Repos / GitHub / GitLab ######################

# Even though the git user.name can be different between repos and
# providers it is best to find a username that is consistent across
# providers and has the same git user.email. If not, stick with one
# provider or override.

havecmd git && export GITUSER="$(git config --global user.name)"

# Detect if the current directory is within a git repository (no matter
# how deep into subdirectories.

isgit () {
  git rev-parse >/dev/null >/dev/null 2>&1
  return $?
} && export -f isgit

# Print the git user.name for the current location.

gituser () { git config user.name; } && export -f gituser

# Print the git user.email for the current location.

gitemail () { git config user.email; } && export -f gitemail

# Print the git user. for the current location.

#gitgpg () { git config TODO???;} && export -f gitgpg

# Print the git remote.origin.url for the current location.

giturl () { git config remote.origin.url; } && export -f giturl

#alias gituser='git config user.name'
#alias gitemail='git config user.email'
#alias gitgpg='git config user.email'
#alias giturl='git config user.email'

# TODO write the comment doc extractor and Pandoc renderer (later)

repo () {
  [[ -z "$REPOS" ]] && telln 'Need to set `$REPOS` first.`' && return 1

  declare manifest="$REPOS/MANIFEST"
  declare subcmd="$1"; shift
  declare server repo rok user url pdir ppath entity

  #TODO seriously consider doing this stuff for every repo subcommand
  # instead of just a select few (most do already).

  case "$subcmd" in
    create|clone|forcedelete|gobadges|delete|show|\
      private|public|listremoteother|listremote|fork|\
      gobadges) 

      
      if [[ -n "$1" ]]; then
        url="$1"
      elif repo inpath; then
        url=$(repo current)
      else
        warnln "Not in $REPOS. Cannot infer repo information."
        return 1
      fi

    #TODO be okay with partial urls in order to infer the service
    #TODO add a default service to .gitconfig (so repo clone does not need
    # full remote path, repo listremote and others need default
    # service, could even make it so partial matches are thrown
    # into a select)

    # TODO grab the git user for each call instead of GITUSER

      server="${url%%/*}"
      repo="${url#*/}"
      entity="${repo%/*}"
      pdir=$(dirpart "$repo")
      reponame=$(basepart "$repo")
      ppath="$REPOS/$pdir"
      tok=$(token "$server")
      ;;
  esac

  case "$subcmd" in

    # Discover descends through the entire $REPOS tree finding Git repos
    # by them having .git directories. It take longer than I would want to
    # do on the command line so it is normally called only from `repo
    # mkmanifest` to create the manifest for faster matches.

    discover) 
      declare dirs=("$REPOS/"**/.git)
      declare dir
      for dir in "${dirs[@]}"; do
        dir=${dir%/.git}
        dir=${dir#$REPOS/}
        echo $dir
      done
      ;;

    # Just simple list of the repos in the current manifest cache.

    list) 
      if [[ ! -e "$manifest" ]]; then
        telln 'Manifest missing, do `repo mkmanifest` to create.'
        return 1
      fi
      katlines "$manifest"
      ;; 

    # Generates a freshly cached $REPOS/MANIFEST file for faster searching
    # and listing. (The full find descent it not particularly responsive.)

    mkmanifest)
      declare dirs=$(repo discover)
      echo '# Generated by `repo mkmanifest`'$(now) >| "$manifest"
      echo >> "$manifest"
      echo "$dirs" >> "$REPOS/MANIFEST" 
      ;;

    # Paths are just the long version of list.

    paths)
      declare repos=($(repo list))
      declare r
      for r in "${repos[@]}"; do
        echo "$REPOS/$r"
      done
      ;;

    # Returns urinary true is current directory is within the $REPOS
    # directory.

    inpath) [[ "$PWD" =~ "$REPOS" ]] && return 0; return 1 ;; 

    # Returns the inferred full repo URL if currently anywhere within a
    # $REPOS subdirectory.

    current) 
      if repo inpath; then
        declare cur="$PWD"
        while [[ "$cur" != "$REPOS" ]]; do
          if [[ -d "$cur/.git" ]]; then
          declare relative=${cur#$REPOS/}
            echo $relative
            return
          else
            cur=${cur%/*}
          fi
        done
      fi
      return 1
    ;;

    # If no arguments are passed at all just changes into the $REPOS
    # directory.

    "") [[ -n "$REPOS" ]] && cd "$REPOS" ;;

    # Matches the keyword or regular expression typed in from the list of
    # repos in the current MANIFEST (repo list).

    match)
      repo list | perg "$1"
      ;;

    # Creates a new repo at the specified URL. The repo at that location
    # will be created and the new repo will be cloned to the local system
    # into the $REPOS directory. Current directory is then changed into
    # the newly cloned directory. Only GitHub is currently supported
    # (github.com).

    create)
      if [[ -e "$REPOS/$url" ]];then
        telln 'Already created: `'$url'`'
        return 1
      fi
      if [[ "$server" == github.com ]]; then
        declare type=user
        if [[ ! "$repo" =~ ^$GITUSER ]];then
          type=orgs/$entity
        fi
        declare -i status=$(curl -X POST -H "Authorization: token $tok" \
          -d '{"name":"'"$reponame"'","private":true}' \
          -w "%{http_code}" \
          -s -o /dev/null \
          "https://api.github.com/$type/repos")
        if [[ $status != 201 ]]; then
          telln '***Failed with status:*** `'$status'`'
          return 1
        fi
        repo clone "$url"
        return
      fi
      telln 'Unsupported Git service or server: `'$server'`'
      return 1
      ;;

    # Clones the URL into the $REPOS directory and changes into the new
    # directory. Also update the local $REPOS/MANIFEST (`repo mkmanifest`)
    # to `match` and `list` return correct results.

    clone)
      if [[ -e "$REPOS/$url" ]];then
        telln 'Already cloned: `'$url'`'
        return 1
      fi
      mkdir -p "$ppath" # gitlab allows sub groups/directories
      git clone git@$server:$repo "$REPOS/$url"
      [[ $? != 0 ]] && return 1
      cd "$REPOS/$url"
      echo "$REPOS/$url"
      repo mkmanifest
      ;;

    # Deletes the specified URL from the remote system and local $REPOS
    # directory. When called with no URL attempts to infer the repo of the
    # current working directory and deletes that repo instead changing
    # into the $REPOS directory when completed.

    delete)
      if [[ "$server" == github.com ]]; then
        danger 'Do you really want to delete `'$server/$repo'`?'
        confirm || return
        repo forcedelete "$*"
        return
      fi
      telln 'Unsupported Git service or server: `'$server'`'
      return 1
      ;;

    # Deletes like `repo delete` but without any confirmation.

    forcedelete)
      if [[ "$server" == github.com ]]; then
        curl -X DELETE -H "Authorization: token $tok" \
          "https://api.github.com/repos/$repo"
        if [[ -d "$REPOS/$url" ]]; then
          cd "$REPOS"
          rm -rf "$REPOS/$url"
          repo mkmanifest
        fi
        return
      fi
      telln 'Unsupported Git service or server: `'$server'`'
      return 1
      ;;

    listremote)
      # TODO conditional on service github
      declare buf=$(curl -X GET -H "Authorization: token $tok" \
        "https://api.github.com/users/$GITUSER/repos" --silent)
      while IFS= read -r line; do
        line=${line//\"/}
        line=${line//\,/}
        line=${line##*:}
        echo $line
      done < <(echo "$buf" | perg full_name)
      return
      ;;

    listremoteother)
      declare buf=$(curl -X GET -H "Authorization: token $tok" \
        "https://api.github.com/user/repos" --silent)
      while IFS= read -r line; do
        line=${line//\"/}
        line=${line//\,/}
        line=${line##*:}
        echo $line
      done < <(echo "$buf" | perg full_name)
      return
      ;;

    # Forks the specified repo creating a repo in the user's remote
    # service collection and then clones it down into $REPOS locally (repo
    # clone).

    fork)
      if [[ "$server" == github.com ]]; then
        curl -X POST -H "Authorization: token $tok" -d '' "https://api.github.com/repos/$repo/forks" 
        repo clone "$*"
      fi
      return
      ;;

    private)
      telln 'Would make private'
      ;;

    public)
      curl -X PATCH -H "Authorization: token $tok" -d '{"private":false}' \
        "https://api.github.com/repos/$repo"
        return 
      ;;

    show)
      if [[ "$server" == github.com ]]; then
         curl -X GET -H "Authorization: token $tok" \
         "https://api.github.com/repos/$repo"
        return
      fi
      telln 'Unsupported Git service or server: `'$server'`'
      return 1
    ;;

    gobadges)
      echo "![WIP](https://img.shields.io/badge/status-wip-red)"
      echo "[![GoDoc](https://godoc.org/$url?status.svg)](https://godoc.org/$url)"
      echo "[![Go Report Card](https://goreportcard.com/badge/$url)](https://goreportcard.com/report/$url)"
      echo "[![Coverage](https://gocover.io/_badge/$url)](https://gocover.io/$url)"
      echo
      return
      ;;

    ping)
      declare server="${1%%/*}"
      ssh "git@$server"
      ;;

    # Uses the mapfile array loading trick to grab all the repos that
    # match the passed arguments. The only different between this and
    # match is that match just returns the match. This prompts for the
    # matching directory to change into if more than one, otherwise it
    # changes into it and echoes the new full path.

    *)
      mapfile -t  matches < <(repo match "$subcmd") # not really a subcmd
      if [[ ${#matches[@]} == 0 ]]; then
        telln 'No repos found.'
        return
      fi
      if [[ ${#matches[@]} == 1 ]]; then
        echo "$REPOS/${matches[0]}"
        cd "$REPOS/${matches[0]}"
        return
      fi
      select one in "${matches[@]}";do
        echo "$REPOS/$one"
        cd "$REPOS/$one"
        break
      done
      ;;

  esac
} && export -f repo && complete -W "list paths discover in mkmanifest
gobadges inpath match create clone delete forcedelete listremote
listremoteother fork private public show ping" repo

# TODO make aware of github as well so can be used for both


#gh () {
#  declare auth="Authorization: token $(token gh)"
#  declare user=$(basepart $(dirpart $PWD))
#  declare repo=$(basepart $PWD)
#  [[ -n "$2" ]] && repo="$2"
#  [[ -n "$3" ]] && user="$3"
#  case "$1" in
#    create)
#      curl -X POST -H "$auth" -d '{"name":"'"$repo"'","private":true}' "https://api.github.com/user/repos"
#      gh init
#      ;;
#    show)
#      curl -X GET -H "$auth" "https://api.github.com/repos/$user/$repo"
#      ;;
#    clone)
#      mkdir -p ~/repos/$user
#      git clone git@github.com:$user/$repo ~/repos/$user/$repo
#      cd ~/repos/$user/$repo
#      ;;
#    fork)
#      curl -X POST -H "$auth" -d '' "https://api.github.com/repos/$user/$repo/forks" 
#      ;;
#    private)
#      curl -X PATCH -H "$auth" -d '{"private":true}' "https://api.github.com/repos/$user/$repo"
#      ;;
#    public)
#      curl -X PATCH -H "$auth" -d '{"private":false}' "https://api.github.com/repos/$user/$repo"
#      ;;
#    ping)
#      ssh git@github.com
#      ;;
#    init)
#      [[ -d .git ]] && warnln 'Already has a `.git`.' && return
#      touch README.md
#      git init
#      git add README.md
#      git commit -m init
#      git remote add origin "git@github.com:$user/$repo"
#      git push -u origin master
#      ;;
#    delete)
#      danger 'Do you really want to delete `'"$user/$repo"'`?' || return
#      curl -X DELETE -H "$auth" "https://api.github.com/repos/$user/$repo"
#      rm -rf "$HOME/repos/$repo"
#      rm -rf "$HOME/go/src/github.com/$user/$repo"
#      ;;
#    gobadges)
#      echo "![WIP](https://img.shields.io/badge/status-wip-red)"
#      echo "[![GoDoc](https://godoc.org/github.com/$user/$repo?status.svg)](https://godoc.org/github.com/$user/$repo)"
#      echo "[![Go Report Card](https://goreportcard.com/badge/github.com/$user/$repo)](https://goreportcard.com/report/github.com/$user/$repo)"
#      echo "[![Coverage](https://gocover.io/_badge/github.com/$user/$repo)](https://gocover.io/github.com/$user/$repo)"
#      echo
#      ;;
#    *)
#      cd ~/repos/github.com/$(git config user.name)
#      ;;
#  esac
#} && export -f gh && complete -W "create show clone fork init delete private public gobadges ping" gh
#

# Passes the arguments on to curl calling the GitLab GraphQL API. Use to
# construct specific service calls.
    #-d '{"query": "'$q'"}' \

curlgl () {
  declare q=$(argsorin $*)
  echo "$q"
  return
  curl -X POST \
    -H "Authorization: Bearer $(token gitlab.com)" \
    -H 'Content-Type: application/json' \
    -d "{\"query\": \"$q\"}" \
    'https://gitlab.com/api/graphql'
  return $?
} && export -f curlgl

lab () {
  case "$1" in
    private)
      echo would change to private;
    ;;
  *)
    echo unimplemented;
    ;;
  esac
} && export -f lab


################## Vim Editing / Magic Wand (!) Functions ################

# Fast way to pull down newly added Plug plugins after update to .vimrc.
# When in doubt begin names with the letter 'p' for disambiguation.

# TODO make a want function that converts Pandoc tables (which are so easy
# to make) into the GitHub Flavored Markdown style tables (that are
# supported everywhere). Need something like that for README.md files that
# are primarily read on GitHub and GitLab instead of getting converted to
# HTML, LaTeX or whatever (which Pandoc focuses on).

alias vimpluginstall="vim +':PlugInstall' +':q!' +':q!'"

# TODO Set the VIMSPELL env variable pointing to the main `add` file so editing

funcsin () {
  egrep '^[-_[:alpha:]]* ?\(' $1 | while read line; do
    echo ${line%%[ (]*}
  done
} && export -f funcsin

aliasesin () {
  mapfile -t  lines < <(perg '^alias ' "$*")
  declare line
  # LEFTOFF
  for line in "${lines[@]}"; do
    echo $line
  done
} && export -f aliasesin

# Opens any executable found in the PATH for editing, including binaries.

vic () {
  vi $(which $1)
} && export -f vic

# Echo the first argument, second argument (n) number of times.

echon () {
  declare i
  for ((i=0; i<${2:-1}; i++)); do echo -n "$1"; done
} && export -f echon

export HRULEWIDTH=74

hrule () {
  declare ch="${1:-#}"
  echo $(echon "$ch" $HRULEWIDTH)
} && export -f hrule

# This is how all the heading separators are made (!!htitle Title)

htitle () {
  declare str=$(argsorin $*)
  declare len=${#str}
  declare side=$(( ((HRULEWIDTH/2)-len/2)-1 ))
  declare left=$side
  declare right=$side
  [[ $[len%2] == 1 ]] && right=$[right-1]
  echo "$(echon '#' $left) $str $(echon '#' $right)"
} && export -f htitle

h1now () { now '#'; } && export -f h1now
h2now () { now '##'; } && export -f h2now
h3now () { now '###'; } && export -f h3now
h4now () { now '####'; } && export -f h4now
h5now () { now '#####'; } && export -f h5now
h6now () { now '######'; } && export -f h6now

######################### Lynx Text Browser FTW! #########################

# This version of lynx does not allow for anything to be passed to it but
# the URL it is to search for. All other configurations must be placed in
# the ~/.config/lynx/lynx.cfg and ~/.config/lynx/lynx.lss files. 

if [ -e "$HOME/.config/lynx/lynx.cfg" ];then
  export LYNX_CFG="$HOME/.config/lynx/lynx.cfg"
fi

if [ -e "$HOME/.config/lynx/lynx.lss" ];then
  export LYNX_LSS="$HOME/.config/lynx/lynx.lss"
fi

export _lynx=$(which lynx)

lynx () { 
  if [ -z "$_lynx" ]; then
      echo "Doesn't look like lynx is installed."
      return 1
  fi
  $_lynx $*
} && export -f lynx 

################### Simple Todo Utility Using Markdown ###################

todo () {
  if [[ -n "$TODOFILE" ]]; then
    $EDITOR $TODOFILE
  else
    telln 'No `$TODOFILE` set.' 
  fi
} && export -f todo

############################# Go Development #############################

# Not being able to use private repos by default with Go is really
# annoying. This is the standard way to overcome that.

export GOPRIVATE="github.com/$GITUSER/*,gitlab.com/$GITUSER/*"
export GOBIN="$HOME/go/bin"
mkdir -p $GOBIN

# Changes into the first Go directory (from GOPATH) matching the name
# passed by looking up the package by specifying the ending string of the
# package name. Prompts for selection if more than one match.  Useful for
# quickly examining the source code of any Go package on the local system.

# (Use of gocd is largely deprecated along with "GOPATH mode" in general.
# Instead keep your go code with all your other repos and use "module
# mode" with `replace` when needed.

gocd () {
  declare q="$1"
  declare list=$(go list -f '{{.Dir}}' ...$q 2>/dev/null)
  IFS=$'\n' declare lines=($list) # split lines
  case "${#lines}" in
    0) tell 'Nothing found for "`'$q'`"' ;;
    1) cd $n ;;
    *) select path in "${lines[@]}"; do cd $path; break; done ;;
  esac
}

# Can be run from TMUX pane to simply watch the tests without having to
# explicitely run them constantly. When combined with a dump() Go utility
# function provides immediate, real-time insight without complicating
# development with a bloated IDE. (Also see Monitoring for other ideas for
# running real-time evaluations during development)

alias goi='go install'
alias gor='go run main.go'
alias got='go test'

gott () {
  declare seconds="$1"
  while true; do 
    go test
    sleep ${seconds:-3}
  done
}

[[ -r ~/.bash_personal   ]] && source ~/.bash_personal
[[ -r ~/.bash_private   ]] && source ~/.bash_private

############################## All Aliases ##############################

# Keep in mind that aliases *cannot* be used from anything but the
# currently active shell (not subprocesses or any system exec() calls).
# Generally exported functions should be used instead (as recommended by
# the bash man page).

alias pie='perl -p -i -e '
alias grep='grep -i --colour=always'
alias egrep='egrep -i --colour=always'
alias fgrep='fgrep -i --colour=always'
alias kali='ssh kali'
alias pico='ssh pico'
alias ssh-keygen="ssh-keygen -t ed25519"
# TODO replace with eval $(twitch bashrc)

alias ignore="twitch chatoff"
alias unignore="twitch chaton"
alias focus="twitch focus"
alias flowstate="twitch flowstate"
alias casual="twitch casual"
alias casualstat="twitch casualstat"
alias chat="twitch chat"
alias chatnicks="twitch chatnicks"

alias raid="twitch raid"
alias c="clear"
alias x="exit"

